package xyz.lexium.staffcore;


import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class UpdateChecker {
	private static UpdateChecker instance = new UpdateChecker();
	private static String BASE_URL = "https://api.spiget.org/v1/resources/";

	@Deprecated
	public static boolean getResource(String name, double currentversion) {
		String url = BASE_URL + name;
		String content = "";
		double version = 0.0D;
		try {
			HttpURLConnection con = createConnection(url);

			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String input;
			while ((input = br.readLine()) != null) {
				content = content + input;
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		JsonObject statistics = new JsonObject();
		JsonParser parser = new JsonParser();
		String json = content;
		statistics = (JsonObject) parser.parse(json);
		try {
			version = statistics.get("version").getAsDouble();
		} catch (Exception e) {
		}
		if (version > currentversion) {
			return true;
		}
		return false;
	}

	public static boolean getResource(int ID, double currentversion) {
		String url = BASE_URL + ID;
		String content = "";
		double version = 0.0D;
		try {
			HttpURLConnection con = createConnection(url);

			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String input;
			while ((input = br.readLine()) != null) {
				content = content + input;
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		JsonObject statistics = new JsonObject();
		JsonParser parser = new JsonParser();
		String json = content;
		statistics = (JsonObject) parser.parse(json);
		try {
			version = statistics.get("version").getAsDouble();
		} catch (Exception e) {
		}
		if (version > currentversion) {
			return true;
		}
		return false;
	}

	private static HttpURLConnection createConnection(String s) throws Exception {
		URL url = new URL(s);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setUseCaches(true);
		connection.addRequestProperty("User-Agent", "Mozilla/4.76");
		connection.setDoOutput(true);
		return connection;
	}

	public static UpdateChecker getInstance() {
		return instance;
	}
}
