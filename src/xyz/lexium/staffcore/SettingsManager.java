package xyz.lexium.staffcore;

import java.io.File;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class SettingsManager {
	
	protected boolean l;


	FileConfiguration config;
	File cfile;

	FileConfiguration banlist;
	File banlistFile;

	public boolean isBanned(Player player) {
		if (getBanList().getBoolean(player.getName().toLowerCase() + ".banned") == false) {
			return false;
		}
		return true;
	}

	public boolean isIPBanned(Player player) {
		if (getBanList().getBoolean(player.getAddress().toString().toLowerCase() + ".banned") == false) {
			return false;
		}
		return true;
	}

	public void setup(Plugin p) {
		cfile = new File(p.getDataFolder(), "config.yml");
		config = p.getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		if (!p.getDataFolder().exists()) {
			p.getDataFolder().mkdir();
		}
	}

	public void loadData(Plugin p) {
		if (getConfig().getBoolean("flatfile")) {
			banlistFile = new File(p.getDataFolder(), "banlist.yml");

			if (!banlistFile.exists()) {
				try {
					banlistFile.createNewFile();
				} catch (IOException e) {
					Bukkit.getServer().getLogger().severe(ChatColor.RED
							+ "Could not create banlist.yml! This is a file that is needed to the plugin will now disable");
					Bukkit.getPluginManager().disablePlugin(p);
				}
			}

			banlist = YamlConfiguration.loadConfiguration(banlistFile);
		}
	}

	public FileConfiguration getBanList() {
		return banlist;
	}

	public void saveBanList() {
		try {
			banlist.save(banlistFile);
		} catch (IOException e) {
			Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save data.yml!");
		}
	}

	public void reloadBanList() {
		banlist = YamlConfiguration.loadConfiguration(banlistFile);
	}

	public FileConfiguration getConfig() {
		return config;
	}

	public void saveConfig() {
		try {
			config.save(cfile);
		} catch (IOException e) {
			Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save config.yml!");
		}
	}

	public void reloadConfig() {
		config = YamlConfiguration.loadConfiguration(cfile);
	}
	
	public void ban(CommandSender sender, String player, String bannedBy, String date, String banMessage) {
		getBanList().set(player.toLowerCase() + ".banned", Boolean.valueOf(true));
		getBanList().set(player.toLowerCase() + ".bannedby", bannedBy);
		getBanList().set(player.toLowerCase() + ".date", date);
		getBanList().set(player.toLowerCase() + ".reason", banMessage);
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.sendMessage(ChatColor.GOLD + "Player: " + player + ChatColor.RED + " has been banned by: " + bannedBy
					+ " for" + " " + banMessage);
		}
		saveBanList();
		String kickMsg = ChatColor.DARK_RED + "You have been banned from this server!\n" + ChatColor.WHITE
				+ "Banned by:  " + ChatColor.RED + bannedBy + ", " + ChatColor.WHITE + "Date: " + ChatColor.RED + date
				+ "\n" + ChatColor.WHITE + "Expires in: NEVER" + "\n" + ChatColor.WHITE + "Reason: " + ChatColor.RED
				+ banMessage;
		Player bannedPlayer = Bukkit.getServer().getPlayer(player);
		if (bannedPlayer != null) {
			bannedPlayer.kickPlayer(kickMsg);
		}

	}

	public void unban(CommandSender sender, String player, String unbannedBy) {
		getBanList().set(player.toLowerCase() + ".banned", Boolean.valueOf(false));
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.sendMessage(ChatColor.GOLD + player + " has been unbanned by " + unbannedBy);
		}
		saveConfig();
	}

	public String getBanReason(String player) {
		return getBanList().getString("bans." + player.toLowerCase() + ".reason");
	}

	public String getBanDate(String player) {
		return getBanList().getString("bans." + player.toLowerCase() + ".date");
	}

	public String getBannedBy(String player) {
		return getBanList().getString("bans." + player.toLowerCase() + ".bannedby");
	}

	public void tempBan(CommandSender sender, String player, String bannedBy, String date, String banMessage,
			String banTime) {
		getBanList().set("bans." + player.toLowerCase() + ".banned", Boolean.valueOf(true));
		getBanList().set("bans." + player.toLowerCase() + ".bannedby", bannedBy);
		getBanList().set("bans." + player.toLowerCase() + ".date", date);
		getBanList().set("bans." + player.toLowerCase() + ".reason", banMessage);
		getBanList().set("bans." + player.toLowerCase() + ".until", banTime);
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.sendMessage(ChatColor.RED + bannedBy + " has temp banned " + player + " for " + banMessage);
		}
		saveConfig();
		this.l = TimeChecker.check(player);
		String kickMsg = ChatColor.DARK_RED + "You have been banned from this server!\n" + ChatColor.WHITE + "Author: "
				+ ChatColor.RED + getBannedBy(player) + ", " + ChatColor.WHITE + "Date: " + ChatColor.RED
				+ getBanDate(player) + "\n" + ChatColor.WHITE + "Expires in: "
				+ ChatColor.RED + (getBanList().getString("bans." + player.toLowerCase() + ".until") != null
						? TimeChecker.getTime() : "NEVER")
				+ "\n" + ChatColor.WHITE + "Reason: " + ChatColor.RED + getBanReason(player);
		Player bannedPlayer = Bukkit.getServer().getPlayer(player);
		if (bannedPlayer != null) {
			bannedPlayer.kickPlayer(kickMsg);
		}
	}

	public void ipBan(CommandSender sender, String ip, String player, String bannedBy, String date, String banMessage) {
		getBanList().set(ip.toLowerCase() + ".banned", Boolean.valueOf(true));
		getBanList().set(ip.toLowerCase() + ".bannedby", bannedBy);
		getBanList().set(ip.toLowerCase() + ".date", date);
		getBanList().set(ip.toLowerCase() + ".reason", banMessage);
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.sendMessage(ChatColor.GOLD + "Player: " + player + ChatColor.RED + " has been banned by: " + bannedBy
					+ " for" + " " + banMessage);
		}
		saveBanList();
		String kickMsg = ChatColor.DARK_RED + "You have been IPBbanned from this server!\n" + ChatColor.WHITE
				+ "Banned by:  " + ChatColor.RED + bannedBy + ", " + ChatColor.WHITE + "Date: " + ChatColor.RED + date
				+ "\n" + ChatColor.WHITE + "Expires in: NEVER" + "\n" + ChatColor.WHITE + "Reason: " + ChatColor.RED
				+ banMessage;
		Player bannedPlayer = Bukkit.getServer().getPlayer(player);
		if (bannedPlayer != null) {
			bannedPlayer.kickPlayer(kickMsg);
		}

	}
}
