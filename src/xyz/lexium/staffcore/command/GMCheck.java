package xyz.lexium.staffcore.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class GMCheck implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		if (lbl.equalsIgnoreCase("gmcheck")) {
			if (sender.hasPermission("staffcore.gmcheck")) {
				if (Bukkit.getPlayer(args[0]) != null) {
					String gm = Bukkit.getPlayer(args[0]).getGameMode().toString();
					sender.sendMessage(
							ChatColor.GOLD + "Player" + Bukkit.getPlayer(args[0]).getName() + " is in gamemode " + gm);
				} else {
					sender.sendMessage(ChatColor.RED + "That player is not online");
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You do not have permission for this command!");
			}
		}
		return false;
	}

}
