package xyz.lexium.staffcore.command;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import xyz.lexium.staffcore.StaffCore;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class Ban implements CommandExecutor {

	String banMessage;
	String bannedBy;
	String date;
	private StaffCore pl;
	public Ban(StaffCore core){
		pl = core;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		if (lbl.equalsIgnoreCase("ban")) {
			if (sender.hasPermission("staffcore.ban")) {
				if (args.length <= 0) {
					if (Bukkit.getPlayer(args[0]) != null) {
						
						// Set the ban message. Loop the array of the command
						if (args.length != 1) {
							this.banMessage = "";
							for (int i = 1; i < args.length; i++) {
								this.banMessage = (this.banMessage + args[i] + " ");
							}
						} else {
							this.banMessage = "The ban hammer has spoken!";
						}
						
						// Gets the person who banned it. if its a player it set it to their name
						// and if its console it set it to console
						if ((sender instanceof Player)) {
							this.bannedBy = (String) sender.getName();
						} else {
							this.bannedBy = "CONSOLE";
						}
						
						// Get the name of the banned player
						String playerName = args[0];
						// Gets the time of the ban
					    Date now = new Date();
					    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					    this.date = format.format(now);
					    pl.getSettings().ban(sender, playerName, lbl, lbl, lbl);
					    sender.sendMessage(ChatColor.GOLD + "You have banned " + playerName + " for " + banMessage);
					} else {
						sender.sendMessage(ChatColor.RED + "That player is not online");
					}
				} else {
					sender.sendMessage(ChatColor.RED + "Invalid command format! " + ChatColor.WHITE
							+ "Correct format: /ban [player] [reason]");
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You do not have permission for this command!");
			}
		}
		return false;
	}

}
