package xyz.lexium.staffcore.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class Kick implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		if (lbl.equalsIgnoreCase("kick")) {
			if (sender.hasPermission("staffcore.kick")) {
				Player kick = (Player) Bukkit.getServer().getPlayer(args[0]);
				if (kick == null) {
					sender.sendMessage("That player is not online :9");
				}
				String message = "";
				if (args.length > 1) {
					for (int i = 1; i < args.length; i++) {
						message = message + args[i] + " ";
					}
				} else {
					message = "You have been kicked";
				}

				String kickMsg = ChatColor.DARK_RED + "You have been kicked from this server!\n"
						+ (message != null ? ChatColor.WHITE + "Reason: " + ChatColor.RED + message : "");

				kick.kickPlayer(kickMsg);
				
			} else {
				sender.sendMessage(ChatColor.RED + "You do not have permission for this command!");
			}
		}
		return false;
	}

}
