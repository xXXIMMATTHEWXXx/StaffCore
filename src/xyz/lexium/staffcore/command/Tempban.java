package xyz.lexium.staffcore.command;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import xyz.lexium.staffcore.StaffCore;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class Tempban implements CommandExecutor {

	String banMessage;
	String bannedBy;
	String date;
	String banTime;

	private StaffCore pl;

	public Tempban(StaffCore core) {
		pl = core;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		if (lbl.equalsIgnoreCase("tempban")) {
			if (sender.hasPermission("staffcore.tempban")) {
				if (args.length == 1) {
					sender.sendMessage(ChatColor.RED + "Invalid command format! " + ChatColor.WHITE
							+ "Correct format: /ban [player] [1s|1m|1h|1d][reason]");
				}
				String timeVariable = args[1].toString().replaceAll("[0-9]", "");
				if (!args[1].toString().matches(".*\\d.*")) {
					sender.sendMessage(ChatColor.RED + "Invalid command format! " + ChatColor.WHITE
							+ "Correct format: /ban [player] [1s|1m|1h|1d][reason]");
				}
				if ((!timeVariable.equalsIgnoreCase("s")) && (!timeVariable.equalsIgnoreCase("m"))
						&& (!timeVariable.equalsIgnoreCase("h")) && (!timeVariable.equalsIgnoreCase("d"))) {
					sender.sendMessage(ChatColor.RED + "Invalid command format! " + ChatColor.WHITE
							+ "Correct format: /ban [player] [1s|1m|1h|1d][reason]");
				}
				if (args.length != 2) {
					this.banMessage = "";
					for (int i = 2; i < args.length; i++) {
						this.banMessage = (this.banMessage + args[i] + " ");
					}
				} else {
					this.banMessage = "The ban hammer has spoken!";
				}

				if (args.length != 1) {
					this.banTime = args[1];
				}
				String playerName = args[0];

				Date now = new Date();
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				this.date = format.format(now);
				if ((sender instanceof Player)) {
					this.bannedBy = ((CommandSender) sender).getName();
				} else {
					this.bannedBy = "CONSOLE";
				}
				pl.getSettings().tempBan(sender, playerName, playerName, lbl, timeVariable, playerName);
			}

		}
		return false;
	}
}