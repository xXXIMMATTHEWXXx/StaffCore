package xyz.lexium.staffcore.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import xyz.lexium.staffcore.StaffCore;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class Unban implements CommandExecutor {

	String bannedBy;
	private StaffCore pl;
	public Unban(StaffCore core) {
		pl = core;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		if (lbl.equalsIgnoreCase("unban")) {
			if (sender.hasPermission("staffcore.unban")){
			    String playerName = args[0];
			    if ((sender instanceof Player)) {
			      this.bannedBy = ((CommandSender)sender).getName();
			    } else {
			      this.bannedBy = "CONSOLE";
			    }
			    pl.getSettings().unban(sender, playerName, sender.getName());
			    sender.sendMessage(ChatColor.GOLD + "You have unbanned!" + playerName);
			}
		}
		return false;
	}

}
