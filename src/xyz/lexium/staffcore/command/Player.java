package xyz.lexium.staffcore.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class Player implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		if (lbl.equalsIgnoreCase("player")) {
			if (sender.hasPermission("staffcore.player")) {
				sender.sendMessage(ChatColor.GOLD + "Showing stats for: " + Bukkit.getPlayer(args[0]).getName());
				sender.sendMessage(ChatColor.GREEN + "Location: " + ChatColor.BLUE
						+ Bukkit.getPlayer(args[0]).getLocation().toString());
				sender.sendMessage(ChatColor.GREEN + "World: " + ChatColor.BLUE + Bukkit.getPlayer(args[0]).getWorld());
				sender.sendMessage(
						ChatColor.GREEN + "Health: " + ChatColor.BLUE + Bukkit.getPlayer(args[0]).getHealth() + "/20");
				sender.sendMessage(
						ChatColor.GREEN + "Food: " + ChatColor.BLUE + Bukkit.getPlayer(args[0]).getFoodLevel());
				sender.sendMessage(
						ChatColor.GREEN + "Flying: " + ChatColor.BLUE + Bukkit.getPlayer(args[0]).isFlying());
				sender.sendMessage(
						ChatColor.GREEN + "Gamemode: " + ChatColor.BLUE + Bukkit.getPlayer(args[0]).getGameMode());
			} else {
				sender.sendMessage(ChatColor.RED + "You do not have permission for this command!");
			}
		}
		return false;
	}

}
