package xyz.lexium.staffcore.command;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.ChatColor;

/*************************************************************************
 * 
 * LEXIUM TECHNOLOGIES lexium.xyz __________________
 * 
 * [2016] - [2020] Lexium Technologies All Rights Reserved.
 * 
 * NOTICE: All information contained herein is, and remains the property of
 * Lexium Technologies and its suppliers, if any. The intellectual and technical
 * concepts contained herein are proprietary to Lexium Technologies and its
 * suppliers and may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law. Dissemination of this
 * information or reproduction of this material is strictly forbidden unless
 * prior written permission is obtained from Lexium Technologies.
 *************************************************************************/

public class Mute extends JavaPlugin implements Listener {
	public static ArrayList<String> muted = new ArrayList<String>();

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		if (label.equalsIgnoreCase("mute")) {
			if (p.hasPermission("staffcore.mute")) {
				if (args.length != 1) {
					p.sendMessage(ChatColor.BLUE + "[Staff] /mute <Player>");
				} else {
					Player target = Bukkit.getPlayer(args[0]);
					if (target != null) {
						if (muted.contains(target.getName())) {
							muted.remove(target.getName());
							p.sendMessage(
									ChatColor.BLUE + "[Staff]" + ChatColor.RED + "You have unmuted" + target.getName());
							target.sendMessage(ChatColor.BLUE + "[Staff]" + ChatColor.RED + "You have been unmuted by"
									+ p.getName());
						} else {
							muted.add(p.getName());
							p.sendMessage(
									ChatColor.BLUE + "[Staff]" + ChatColor.RED + "cYou have muted" + target.getName());
							target.sendMessage(
									ChatColor.BLUE + "[Staff]" + ChatColor.RED + "You are muted from" + p.getName());
						}
					} else {
						p.sendMessage(ChatColor.BLUE + "[Staff]" + ChatColor.RED + "This player isn't" + ChatColor.GREEN + "online");
					}
				}
			} else {
				p.sendMessage(
						ChatColor.BLUE + "[Staff]" + ChatColor.RED + "You do not have permission for this command!");
			}
		}
		return true;
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player p = e.getPlayer();
		if (muted.contains(p.getName())) {
			e.setCancelled(true);
			p.sendMessage( ChatColor.BLUE + "[Staff]" +  ChatColor.RED + "You are muted!");
		} else {
			e.setCancelled(false);
		}
	}
}
