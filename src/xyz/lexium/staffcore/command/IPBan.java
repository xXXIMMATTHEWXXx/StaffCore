package xyz.lexium.staffcore.command;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import xyz.lexium.staffcore.StaffCore;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class IPBan implements CommandExecutor {

	String date;
	String banMessage;
	private StaffCore pl;

	public IPBan(StaffCore core) {
		pl = core;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args) {
		if (lbl.equalsIgnoreCase("ipban")) {
			if (sender.hasPermission("staffcore.ipban")) {
				int length = args.length;
				if (length == 2) {
					if (Bukkit.getPlayer(args[0]) != null) {
						String ipaddress = Bukkit.getPlayer(args[0]).getAddress().toString().split(":")[0]
								.split("/")[1];
					    Date now = new Date();
					    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
					    this.date = format.format(now);
					    
						if (args.length != 1) {
							this.banMessage = "";
							for (int i = 1; i < args.length; i++) {
								this.banMessage = (this.banMessage + args[i] + " ");
							}
						} else {
							this.banMessage = "The ban hammer has spoken!";
						}
						
						pl.getSettings().ipBan(sender, ipaddress, args[0], sender.getName(), date, banMessage);
					} else {
						sender.sendMessage(ChatColor.RED + "That player is not online :9");
					}
				}
				if (length == 1) {
					sender.sendMessage(ChatColor.RED + "The correct usage is /ipban <player> [reason]");
				}
				if (length == 0) {
					sender.sendMessage(ChatColor.RED + "The correct usage is /ipban <player> [reason]");
				}
			} else {
				sender.sendMessage(ChatColor.RED + "You do not have permission for this command!");
			}
		}
		return false;
	}

}
