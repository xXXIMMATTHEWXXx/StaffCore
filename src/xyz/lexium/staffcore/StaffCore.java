package xyz.lexium.staffcore;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import org.bukkit.plugin.java.JavaPlugin;
import com.google.common.base.Stopwatch;
import xyz.lexium.staffcore.command.Ban;
import xyz.lexium.staffcore.command.Clearchat;
import xyz.lexium.staffcore.command.GMCheck;
import xyz.lexium.staffcore.command.IPBan;
import xyz.lexium.staffcore.command.Kick;
import xyz.lexium.staffcore.command.Mute;
import xyz.lexium.staffcore.command.Player;
import xyz.lexium.staffcore.command.Tempban;
import xyz.lexium.staffcore.command.Unban;


/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class StaffCore extends JavaPlugin {

	Logger logger = getLogger();
	SettingsManager manager = new SettingsManager();

	public void onEnable() {
		Stopwatch stopwatch = Stopwatch.createStarted();
		manager.setup(this);
		registerCommands();
		manager.loadData(this);
		checkForUpdates();
		stopwatch.stop();
		long time = stopwatch.elapsed(TimeUnit.MILLISECONDS);
		logger.info("Enabled! " + time);
	}

	public void onDisable() {
		Stopwatch stopwatch = Stopwatch.createStarted();

		stopwatch.stop();
		long time = stopwatch.elapsed(TimeUnit.MILLISECONDS);
		logger.info("Diasabled! " + time);
	}

	private void registerCommands() {
		getCommand("ban").setExecutor(new Ban(this));
		getCommand("unban").setExecutor(new Unban(this));
		getCommand("clearchat").setExecutor(new Clearchat());
		getCommand("gmcheck").setExecutor(new GMCheck());
		getCommand("ipban").setExecutor(new IPBan(this));
		getCommand("kick").setExecutor(new Kick());
		getCommand("mute").setExecutor(new Mute());
		getCommand("player").setExecutor(new Player());
		getCommand("tempban").setExecutor(new Tempban(this));
	}
	
	public String format(String s){
		return s.replace('&', '�');
	}

	public void checkForUpdates() {
		//if (UpdateChecker.getResource(17608, 1.0)) {
		//	Bukkit.getServer().getConsoleSender()
		//			.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "There's a new update avaliable for StaffCore");
		//}
	}

	public SettingsManager getSettings() {
		return manager;
	}
	
	
	
}
