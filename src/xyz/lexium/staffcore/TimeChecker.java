package xyz.lexium.staffcore;

import java.text.SimpleDateFormat;
import java.util.Date;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class TimeChecker {
	static String seconds;
	static String minutes;
	static String hours;
	static String days;
	static String seconds1;
	static String minutes1;
	static String hours1;
	static String days1;
	static int leftSide;
	static int rightSide;

	public static boolean check(String player) {
		String banDate = SettingsManager.getInstance().getConfig().getString("bans." + player.toLowerCase() + ".date");
		banDate = banDate.replace(":", " ").replace("-", " ");

		String[] newString = banDate.split(" ");

		seconds = newString[5];
		minutes = newString[4];
		hours = newString[3];
		days = newString[0];

		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String date = format.format(now);
		date = date.replace(":", " ").replace("-", " ");

		String[] newString1 = date.split(" ");

		seconds1 = newString1[5];
		minutes1 = newString1[4];
		hours1 = newString1[3];
		days1 = newString1[0];

		String until = SettingsManager.getInstance().getConfig().getString("bans." + player.toLowerCase() + ".until");

		String integer1 = until.replaceAll("[^\\d.]", "");
		int timeUntilUnban = Integer.parseInt(integer1);
		if (!until.contains("s")) {
			if (until.contains("m")) {
				timeUntilUnban *= 60;
			} else if (until.contains("h")) {
				timeUntilUnban = timeUntilUnban * 60 * 60;
			} else if (until.contains("d")) {
				timeUntilUnban = timeUntilUnban * 60 * 60 * 24;
			}
		}
		int seconds2 = Integer.parseInt(seconds);
		int minutes2 = Integer.parseInt(minutes);
		int hours2 = Integer.parseInt(hours);
		int days2 = Integer.parseInt(days);

		int seconds3 = Integer.parseInt(seconds1);
		int minutes3 = Integer.parseInt(minutes1);
		int hours3 = Integer.parseInt(hours1);
		int days3 = Integer.parseInt(days1);

		leftSide = seconds2 + minutes2 * 60 + hours2 * 60 * 60 + days2 * 60 * 60 * 24 + timeUntilUnban;
		rightSide = seconds3 + minutes3 * 60 + hours3 * 60 * 60 + days3 * 60 * 60 * 24;
		if (leftSide >= rightSide) {
			return true;
		}
		SettingsManager.getInstance().getConfig().set("bans." + player.toLowerCase() + ".until", null);
		SettingsManager.getInstance().getConfig().set("bans." + player.toLowerCase() + ".banned",
				Boolean.valueOf(false));
		return false;
	}

	public static String getTime() {
		int timer = leftSide - rightSide;
		int convertedDays = timer / 86400;
		int remainder1 = timer % 86400;
		int convertedHours = remainder1 / 3600;
		int remainder = timer % 3600;
		int convertedMinutes = remainder / 60;
		int convertedSeconds = remainder % 60;

		String formattedTime = (convertedDays != 0 ? convertedDays + "days "
				: convertedDays == 1 ? convertedDays + "day " : "")
				+ (convertedHours != 0 ? convertedHours + "hours "
						: convertedHours == 1 ? convertedHours + "hour " : "")
				+ (convertedMinutes != 0 ? convertedMinutes + "minutes "
						: convertedMinutes == 1 ? convertedMinutes + "minute " : "")
				+ (convertedSeconds != 0 ? convertedSeconds + "seconds"
						: convertedSeconds == 1 ? convertedSeconds + "second" : "");

		return formattedTime;
	}
}
