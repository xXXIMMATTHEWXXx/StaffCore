package xyz.lexium.staffcore.events;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import xyz.lexium.staffcore.StaffCore;
import xyz.lexium.staffcore.TimeChecker;

/*************************************************************************
* 
* LEXIUM TECHNOLOGIES
* lexium.xyz
* __________________
* 
*  [2016] - [2020] Lexium Technologies
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Lexium Technologies and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Lexium Technologies
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Lexium Technologies.
*************************************************************************/

public class PlayerLogin implements Listener {

	private StaffCore pl;

	public PlayerLogin(StaffCore core) {
		pl = core;
	}

	@EventHandler
	public void onJoin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		String expire = null;

		expire = pl.getSettings().getBanList().getString(player.getName().toLowerCase() + ".until") != null
				? TimeChecker.getTime() : "NEVER" + "\n";
		if (pl.getSettings().isBanned(player)) {
			String kickMsg = ChatColor.DARK_RED + "You have been banned from this server!\n" + ChatColor.WHITE
					+ "Banned by:  " + ChatColor.RED
					+ pl.getSettings().getBanList().getString(player.getName().toLowerCase() + ".bannedby") + ", "
					+ ChatColor.WHITE + "Date: " + ChatColor.RED
					+ pl.getSettings().getBanList().getString(player.getName().toLowerCase() + ".date") + "\n"
					+ ChatColor.WHITE + "Expires in: " + expire + "\n" + ChatColor.WHITE + "Reason: " + ChatColor.RED
					+ pl.getSettings().getBanList().getString(player.getName().toLowerCase() + ".reason");

			event.disallow(PlayerLoginEvent.Result.KICK_OTHER, kickMsg);

			if (pl.getSettings().isIPBanned(player)) {
				String kickMsg2 = ChatColor.DARK_RED + "You have been IPBanned from this server!\n" + ChatColor.WHITE
						+ "Banned by:  " + ChatColor.RED
						+ pl.getSettings().getBanList().getString(player.getName().toLowerCase() + ".bannedby") + ", "
						+ ChatColor.WHITE + "Date: " + ChatColor.RED
						+ pl.getSettings().getBanList().getString(player.getName().toLowerCase() + ".date") + "\n"
						+ ChatColor.WHITE + "Expires in: " + expire + "\n" + ChatColor.WHITE + "Reason: " + ChatColor.RED
						+ pl.getSettings().getBanList().getString(player.getName().toLowerCase() + ".reason");

				event.disallow(PlayerLoginEvent.Result.KICK_OTHER, kickMsg2);
			}
		}
	}
}
